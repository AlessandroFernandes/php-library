<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Dia <?php echo date('d'); ?></title>
	<link rel="stylesheet" href="">

	<?php
	function linha($semana){
		echo "<tr>";
		for ($i = 0; $i <=6; $i++){
			if(isset($semana[$i])){
				echo "<td>{$semana[$i]}</td>";
			}else{
				echo "<td></td>";
			}
		}
		echo "</tr>";
	}
	function calendario(){
		$dia = 1;
		$semana = array();
		while ($dia <= 31){
			array_push($semana, $dia);

			if(count($semana) == 7){
				linha($semana);
				$semana = array();
			}
			$dia++;
		}
		linha($semana);
	}
 	?>

</head>
<body>
	
		<h1> Estamos em <?php echo date('Y'); ?></h1>
		<p>
			Agora são <?php echo date('H'); ?> horas
			 e <?php echo date('i'); ?> minutos.
		</p>
		<table border="1">
			<tr>
				<th>Dom</th>
				<th>Seg</th>
				<th>Ter</th>
				<th>Qua</th>
				<th>Qui</th>
				<th>Sex</th>
				<th>Sáb</th>
			</tr>
			<?php calendario(); ?>
</body>
</html>

<h1><?php echo "Título dentro do php" ?></h1>