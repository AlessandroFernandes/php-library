<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Titulo Exportado</title>
	<link rel="stylesheet" href="../css/paraPhp.css">
</head>
<body>
<h1>Gerenciador de Tarefas</h1>
	<form>
		<fieldset>
			<legend>Nova tarefa</legend>
			<label>
				Tarefa: 
				<input type="text" name="nome" value="" placeholder="Digita a Tarefa">
			</label>
		</br>		
		<label>
			Descrição (Opcional):
			<textarea name="descricao"></textarea>
		</label>
		</br>
		<label>
			Prazo (Opcional):
			<input type="text" name="prazo" value="" placeholder="">
		</label>
		</br>
		<fieldset class="fiel">
			<legend>Prioridade</legend>
			<label>
				<input type="radio" name="prioridade" value="baixa" checked />
				Baixa
				<input type="radio" name="prioridade" value="media" />
				Média
				<input type="radio" name="prioridade" value="alta" />
				Alta
			</label>
		</fieldset>
		
		<label>
			Tarefa concluída:
			<input type="checkbox" name="concluida" value="sim">
		</label>
		</br>
		<input type="submit" name="" value="Registrar">
		</fieldset>

	</form>
	<table>
		<tr>
			<th>Tarefas</th>
			<th>Descricao</th>
			<th>Prazo</th>
			<th>Prioridade</th>
			<th>Concluída</th>
		</tr>
		<?php foreach ($lista_tarefas as $tarefa) : ?>
			<tr>
				<td><?php echo $tarefa['nome'] ?></td>
				<td><?php echo $tarefa['descricao'] ?></td>
				<td><?php echo $tarefa['prazo'] ?></td>
				<td><?php echo $tarefa['prioridade'] ?></td>
				<td><?php echo $tarefa['concluida'] ?></td>
			</tr>
		<?php endforeach; ?>
	</table>
</body>
</html>