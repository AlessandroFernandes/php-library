

				function moneyTextToFloat(text){
					var cleanText = text.replace("R$","").replace(",",".");
					return parseFloat(cleanText);
				}

				function floatToMoneyText(value){
					var text = (value < 1 ? "0" : "") + Math.floor(value * 100);
					text = "R$ " + text;
					return text.substr(0, text.length - 2) + "," + text.substr(-2);
				}

			
				function readTotal(){
					var total = $("#total").text();
					return moneyTextToFloat(total);
				}

				function writeTotal(value){
					var total = floatToMoneyText(value);
					$("#total").text(total);
				}
				
				function finalCalculation(){

					var products = $(".product");
					var calculation = 0;

					$(products).each(function(x, product){

						var $product = $(product);
						var quantity = moneyTextToFloat($product.find(".quantity").val());
						var price = moneyTextToFloat($product.find(".price").text());
						calculation += quantity * price;

					});
					return calculation;
				}
				

				function valueChanged(){
					writeTotal(finalCalculation());
				}

				$(function(){
					$(".quantity").change(function() {
						writeTotal(finalCalculation());
					});
					/*
					var textDocument = document.getElementsByClassName("quantity");

					for(var x = 0; x < textDocument.length; x++){
						textDocument[x].onchange = function(){
							writeTotal(finalCalculation());
						}
					}*/
				});
				
  				
			
