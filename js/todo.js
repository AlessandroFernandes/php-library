$(function(){

var $lastClicked;
var meu_login = "seu@email.com";
var server = "http://livro-capitulo07.herokuapp.com";

$(".tarefa-delete").addClass('glyphicon glyphicon-trash');

function onDeleteObjeto(){
		
		$(this).parent(".tarefa-item").off("click").hide("slow", function(){
			$(this).remove();
		});
	}

function onTarefaItem(){
			

			if(!$(this).is($lastClicked)){
				if($lastClicked !== undefined){
					savePendingEdition($lastClicked);
				}
			
			$lastClicked = $(this);

			var text = $lastClicked.children(".tarefa-texto").text();
			var id = $lastClicked.children('.tarefa-id').text();
			var html = "<div class='tarefa-id'>" + id + "</div>" +
			 "<input type='text' class='tarefa-edit' value='"+
			 text +"'>";
			
			$lastClicked.html(html);
			$(".tarefa-edit").keydown(onKeyDown);

		}

	}

function onKeyDown(event){
		if(event.which === 13){
			savePendingEdition($lastClicked);
			$lastClicked = undefined;
		};
	}

function onTarefaKeyDown(event){
		if(event.which === 13){
			addTarefa($("#tarefa").val());
			$("#tarefa").val("");
		};
	}

function savePendingEdition($tarefa){
		var texto = $tarefa.children('.tarefa-edit').val();
		var id = $tarefa.children('.tarefa-id').text();
		$tarefa.empty();

		/*$tarefa.append("<div class='tarefa-texto'>"+ texto +"</div>")
		.append("<div class='tarefa-delete'></div>")
		.append("<div class='clear'></div>");*/

		$tarefa.append($("<div />").addClass('tarefa-id').text(id))
		.append($("<div />").addClass('tarefa-texto').text(texto))
		.append($("<div />").addClass('tarefa-delete'))
		.append($("<div />").addClass('clear'));

		updateTarefa(text, id);

		$(".tarefa-delete").click(onDeleteObjeto);
		
		$tarefa.click(onTarefaItem);

	}

function addTarefa(text, id){

		id = id || 0;

		var $tarefa = $("<div />").addClass('tarefa-item')
		.append($("<div />").addClass('tarefa-id').text(id))
		.append($("<div />").addClass('tarefa-texto').text(text))
		.append($("<div />").addClass('tarefa-delete').addClass('glyphicon glyphicon-trash'))
		.append($("<div />").addClass('clear'));

	$("#tarefa-lista").append($tarefa);
	$(".tarefa-delete").click(onDeleteObjeto);
	$(".tarefa-item").click(onTarefaItem);

	if(id === 0){
		var div = $($tarefa.children(".tarefa-id"));
		console.log("id", div);
		newTarefa(text, $(div));
	}

	}
	$(".tarefa-delete").click(onDeleteObjeto);
	$(".tarefa-item").click(onTarefaItem);
	$("#tarefa").keydown(onTarefaKeyDown);

	var parametros = {nome: "caro leitor"};
	var servico = "http://livro-capitulo07.herokuapp.com/hello";
	$.get(servico, parametros).done(function(data){
		alert(data);
	});

	var servico2 = "http://livro-capitulo07.herokuapp.com/error";
	$.get(servico2).fail(function(data){
		alert(data.responseText);
	});
	var servico3 = "http://api.postmon.com.br/v1/cep/";
	var cep = "04101-555";

function onCepDone(data){
		console.log("A casa do'código fica na " + data.logradouro);
	}

function onCepError(error){
		console.log("erro: " + error.statusText);
	}

	$.getJSON(servico3 + cep).done(onCepDone).fail(onCepError);;

	


function loadTarefas(){
	$("#tarefa").empty();

	$.getJSON(server +"/tarefas", {usuario: meu_login})
	.done(function(data){
		console.log("data: ", data);
		for(var tarefa = 0;tarefa < data.length; tarefa++){
			addTarefa(data[tarefa].texto, data[tarefa].id);
		}
	});
}

function updateTarefa(text,id){
	$.post(server + "/tarefa", {tarefa_id: id, texto: text});
}

function newTarefa(text, $div){
	$.post(server + "/tarefa", {usuario: meu_login, texto: text, _method: "PUT"})
	.done(function(data){
		$div.text(data.id);
	});
}

})